package co.stone.loggingkt.handlers

import co.stone.loggingkt.loggers.Severity

interface LogHandler {
    fun handle(owner: String, severity: Severity, message: String, traceId: Long, tags: Iterable<String>, custom: Iterable<Pair<String, Any?>>)
}