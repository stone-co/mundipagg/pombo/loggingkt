package co.stone.loggingkt.handlers.cerberus

import co.stone.loggingkt.loggers.Severity
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer

internal object LevelSerializer : StdSerializer<Severity>(Severity::class.java) {
    override fun serialize(value: Severity, gen: JsonGenerator, provider: SerializerProvider) {
        gen.writeString(value.name.toLowerCase().capitalize())
    }
}