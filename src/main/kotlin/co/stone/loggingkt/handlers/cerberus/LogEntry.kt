package co.stone.loggingkt.handlers.cerberus

import co.stone.loggingkt.loggers.Severity
import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonProperty
import org.joda.time.DateTime
import java.net.InetAddress

data class LogEntry(
        val severity: Severity,
        val message: String,
        val timestamp: DateTime,
        val tags: Iterable<String>,
        val productCompany: String,
        val productName: String,
        val productVersion: String,
        val additionalData: Map<String, Any?>? = null
) {
    val typeName: String = "${severity.name.toLowerCase().capitalize()}Entry"
    val managedThreadId = Thread.currentThread().id.toString()
    val managedThreadName = Thread.currentThread().name
    val nativeThreadId = managedThreadId
    val nativeProcessId = try {
        val runtime = java.lang.management.ManagementFactory.getRuntimeMXBean()
        val jvm = runtime.javaClass.getDeclaredField("jvm")
        jvm.isAccessible = true
        val manager = jvm.get(runtime) as sun.management.VMManagement
        val pid = manager.javaClass.getDeclaredMethod("getProcessId")
        pid.isAccessible = true
        (pid.invoke(manager) as Int).toString()
    } catch (e: Exception) {
        "1"
    }
    val machineName = try {
        InetAddress.getLocalHost().hostName
    } catch (ex: Exception) {
        "hostname"
    }
    @JsonAlias("OSFullName")
    @JsonProperty("OSFullName")
    val OSFullName = "${
    System.getProperty("os.name")
    } ${
    System.getProperty("os.version")
    } ${
    System.getProperty("os.arch")
    }"
    val processName = System.getProperty("java.vm.name")
    val processPath = System.getProperty("user.dir")
}