package co.stone.loggingkt.handlers.cerberus

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import org.joda.time.DateTime

internal object DatetimeSerializer : StdSerializer<DateTime>(DateTime::class.java) {
    override fun serialize(value: DateTime, gen: JsonGenerator, provider: SerializerProvider) {
        gen.writeString(value.toString("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZZ"))
    }
}