package co.stone.loggingkt.handlers.cerberus

import co.stone.loggingkt.async.Every
import co.stone.loggingkt.async.LockWithMutex
import co.stone.loggingkt.async.Lockable
import co.stone.loggingkt.exceptions.neverThrows
import co.stone.loggingkt.handlers.LogHandler
import co.stone.loggingkt.loggers.Severity
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.github.kittinunf.fuel.httpPost
import kotlinx.coroutines.experimental.launch
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.Duration
import java.util.ArrayDeque

class CerberusHandler(
        private val url: String,
        private val company: String,
        private val application: String,
        private val version: String,
        private val bufferSize: Int = 100,
        refreshRate: Int = 10,
        private val minimalSeverity: Severity = Severity.DEBUG
) : LogHandler, Lockable by LockWithMutex() {
    private val buffer = ArrayDeque<LogEntry>()
    private val mapper = ObjectMapper().apply {
        registerModule(KotlinModule())
        registerModule(SimpleModule().apply {
            addSerializer(LevelSerializer)
            addSerializer(DatetimeSerializer)
        })
        propertyNamingStrategy = PropertyNamingStrategy.UPPER_CAMEL_CASE
    }
    private val cron = Every(Duration.millis(refreshRate * 1000L), this::sendBuffer).apply {
        start()
    }

    override fun handle(owner: String, severity: Severity, message: String, traceId: Long, tags: Iterable<String>, custom: Iterable<Pair<String, Any?>>) {
        if (minimalSeverity.identifier >= severity.identifier) {
            val log = LogEntry(
                    productVersion = version,
                    productCompany = company,
                    productName = application,
                    severity = severity,
                    message = message,
                    timestamp = DateTime.now(DateTimeZone.UTC),
                    tags = tags + listOf(owner),
                    additionalData = custom.toMap()
            )
            lock {
                buffer.push(log)
            }
            launch {
                lock {
                    if (buffer.size >= bufferSize) {
                        sendBuffer()
                    }
                }
            }
        }
    }


    private fun sendBuffer() = neverThrows {
        val body = mapper.writeValueAsString(buffer.map { it })
        buffer.clear()
        this.url
                .httpPost()
                .header("Content-Type" to "application/json")
                .header("Accept" to "application/json")
                .body(body)
                .response()
    }
}
