package co.stone.loggingkt.handlers.standard

import co.stone.loggingkt.handlers.LogHandler
import co.stone.loggingkt.loggers.Severity
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.ISODateTimeFormat

/**
 * A handler that writes each log entry on the [output] with the given [template]
 * unless the log entry has a severity smaller than the [minimalSeverity]
 */
class FileHandler(
        private val output: Appendable,
        private val template: String = "{timestamp}||{severity}||{message}||{tags}||{custom}\n",
        private val minimalSeverity: Severity = Severity.DEBUG
) : LogHandler {
    private val formatter = ISODateTimeFormat.dateTime()!!

    override fun handle(owner: String, severity: Severity, message: String, traceId: Long, tags: Iterable<String>, custom: Iterable<Pair<String, Any?>>) {
        if (minimalSeverity.identifier >= severity.identifier) {
            var log = template.replace("{timestamp}", DateTime.now(DateTimeZone.UTC).toString(formatter))
            log = log.replace("{severity}", severity.name.toLowerCase())
            log = log.replace("{message}", message)
            log = log.replace("{trace}", traceId.toString())
            log = log.replace("{tags}", "[${tags.joinToString(", ")}]")
            log = log.replace("{custom}", "[${custom.joinToString(", ") { "${it.first} -> ${it.second}" }}]")
            output.append(log)
        }
    }
}