package co.stone.loggingkt.handlers.slf4j

import co.stone.loggingkt.handlers.LogHandler
import co.stone.loggingkt.loggers.Severity
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.ISODateTimeFormat
import org.slf4j.LoggerFactory

class SLF4JHandler(
        private val template: String = "{timestamp}  {message}  {tags}  {custom}",
        private val minimalSeverity: Severity = Severity.DEBUG
) : LogHandler {
    private val formatter = ISODateTimeFormat.dateTime()!!

    override fun handle(owner: String, severity: Severity, message: String, traceId: Long, tags: Iterable<String>, custom: Iterable<Pair<String, Any?>>) {
        if (minimalSeverity.identifier >= severity.identifier) {
            val logger = LoggerFactory.getLogger(owner)
            var log = template.replace("{timestamp}", DateTime.now(DateTimeZone.UTC).toString(formatter))
            log = log.replace("{message}", message)
            log = log.replace("{trace}", traceId.toString())
            log = log.replace("{tags}", "[${tags.joinToString(", ")}]")
            log = log.replace("{custom}", "[${custom.joinToString(", ") { "${it.first} -> ${it.second}" }}]")
            when (severity) {
                Severity.INFO ->
                    logger.info(log)
                Severity.NOTICE ->
                    logger.info(log)
                Severity.DEBUG ->
                    logger.debug(log)
                Severity.WARNING ->
                    logger.warn(log)
                Severity.ALERT ->
                    logger.warn(log)
                Severity.CRITICAL ->
                    logger.warn(log)
                else -> logger.error(log)
            }
        }
    }
}