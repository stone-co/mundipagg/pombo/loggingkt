package co.stone.loggingkt

import co.stone.loggingkt.handlers.LogHandler
import co.stone.loggingkt.loggers.Logger
import co.stone.loggingkt.loggers.LoggerFactory
import kotlin.reflect.KClass

object LoggingKt {
    fun new(owner: Class<*>): Logger {
        return LoggerFactory.new(owner)
    }

    fun new(owner: KClass<*>): Logger {
        return LoggerFactory.new(owner)
    }

    fun new(owner: String = "shared"): Logger {
        return LoggerFactory.new(owner)
    }

    fun addHandler(handler: LogHandler) {
        LoggerFactory.addHandler(handler)
    }

    operator fun LogHandler.unaryPlus() {
        this@LoggingKt.addHandler(this)
    }

    inline fun <T> handlers(call: LoggingKt.() -> T) {
        this.call()
    }
}