package co.stone.loggingkt.loggers

import java.io.PrintWriter
import java.io.StringWriter


interface Logger {
    val Exception.stackTraceString: String
        get() {
            val sw = StringWriter()
            val pw = PrintWriter(sw)
            this.printStackTrace(pw)
            return sw.toString()
        }

    fun debug(
            message: String,
            tags: Iterable<String> = listOf(),
            custom: Iterable<Pair<String, Any?>> = listOf()
    ) {
        log(Severity.DEBUG, message, tags, custom)
    }

    fun debug(
            message: String,
            vararg tags: String
    ) {
        debug(message, tags.toList())
    }

    fun debug(
            message: String,
            vararg custom: Pair<String, Any?>
    ) {
        debug(message, custom = custom.toList())
    }

    fun debug(
            message: String,
            vararg tags: String,
            custom: () -> Iterable<Pair<String, Any?>>
    ) {
        debug(message, tags.toList(), custom())
    }

    fun info(
            message: String,
            tags: Iterable<String> = listOf(),
            custom: Iterable<Pair<String, Any?>> = listOf()
    ) {
        log(Severity.INFO, message, tags, custom)
    }

    fun info(
            message: String,
            vararg tags: String
    ) {
        info(message, tags.toList())
    }

    fun info(
            message: String,
            vararg custom: Pair<String, Any?>
    ) {
        info(message, custom = custom.toList())
    }

    fun info(
            message: String,
            vararg tags: String,
            custom: () -> Iterable<Pair<String, Any?>>
    ) {
        info(message, tags.toList(), custom())
    }

    fun notice(
            message: String,
            tags: Iterable<String> = listOf(),
            custom: Iterable<Pair<String, Any?>> = listOf()
    ) {
        log(Severity.NOTICE, message, tags, custom)
    }

    fun notice(
            message: String,
            vararg tags: String
    ) {
        notice(message, tags.toList())
    }

    fun notice(
            message: String,
            vararg custom: Pair<String, Any?>
    ) {
        notice(message, custom = custom.toList())
    }

    fun notice(
            message: String,
            vararg tags: String,
            custom: () -> Iterable<Pair<String, Any?>>
    ) {
        notice(message, tags.toList(), custom())
    }

    fun warn(
            message: String,
            tags: Iterable<String> = listOf(),
            custom: Iterable<Pair<String, Any?>> = listOf()
    ) {
        log(Severity.WARNING, message, tags, custom)
    }

    fun warn(
            message: String,
            vararg tags: String
    ) {
        warn(message, tags.toList())
    }

    fun warn(
            message: String,
            vararg custom: Pair<String, Any?>
    ) {
        warn(message, custom = custom.toList())
    }

    fun warn(
            message: String,
            vararg tags: String,
            custom: () -> Iterable<Pair<String, Any?>>
    ) {
        notice(message, tags.toList(), custom())
    }

    fun error(
            message: String,
            tags: Iterable<String> = listOf(),
            custom: Iterable<Pair<String, Any?>> = listOf()
    ) {
        log(Severity.ERROR, message, tags, custom)
    }

    fun error(
            message: String,
            vararg tags: String
    ) {
        error(message, tags.toList())
    }

    fun error(
            message: String,
            vararg custom: Pair<String, Any?>
    ) {
        error(message, custom = custom.toList())
    }

    fun error(
            message: String,
            vararg tags: String,
            custom: () -> Iterable<Pair<String, Any?>>
    ) {
        error(message, tags.toList(), custom())
    }

    fun error(
            message: String,
            ex: Exception,
            tags: Iterable<String> = listOf(),
            custom: Iterable<Pair<String, Any?>> = listOf()
    ) {
        error(message, tags, custom + listOf(
                "exception" to ex::class.qualifiedName,
                "exceptionMessage" to ex.message,
                "stackTrace" to ex.stackTraceString
        ))
    }

    fun error(
            message: String,
            ex: Exception,
            vararg tags: String
    ) {
        error(message, ex, tags.toList())
    }

    fun error(
            message: String,
            ex: Exception,
            vararg custom: Pair<String, Any?>
    ) {
        error(message, ex, custom = custom.toList())
    }

    fun error(
            message: String,
            ex: Exception,
            vararg tags: String,
            custom: () -> Iterable<Pair<String, Any?>>
    ) {
        error(message, ex, tags.toList(), custom())
    }

    fun error(
            ex: Exception,
            tags: Iterable<String> = listOf(),
            custom: Iterable<Pair<String, Any?>> = listOf()
    ) {
        error("The exception '${ex::class.qualifiedName}' was raised!", ex, tags, custom)
    }

    fun error(
            ex: Exception,
            vararg tags: String
    ) {
        error(ex, tags.toList())
    }

    fun error(
            ex: Exception,
            vararg custom: Pair<String, Any?>
    ) {
        error(ex, custom = custom.toList())
    }

    fun error(
            ex: Exception,
            vararg tags: String,
            custom: () -> Iterable<Pair<String, Any?>>
    ) {
        error(ex, tags.toList(), custom())
    }

    fun critical(
            message: String,
            tags: Iterable<String> = listOf(),
            custom: Iterable<Pair<String, Any?>> = listOf()
    ) {
        log(Severity.CRITICAL, message, tags, custom)
    }

    fun critical(
            message: String,
            vararg tags: String
    ) {
        critical(message, tags.toList())
    }

    fun critical(
            message: String,
            vararg custom: Pair<String, Any?>
    ) {
        critical(message, custom = custom.toList())
    }

    fun critical(
            message: String,
            vararg tags: String,
            custom: () -> Iterable<Pair<String, Any?>>
    ) {
        critical(message, tags.toList(), custom())
    }

    fun critical(
            message: String,
            ex: Exception,
            tags: Iterable<String> = listOf(),
            custom: Iterable<Pair<String, Any?>> = listOf()
    ) {
        critical(message, tags, custom + listOf(
                "exception" to ex::class.qualifiedName,
                "exceptionMessage" to ex.message,
                "stackTrace" to ex.stackTraceString
        ))
    }

    fun critical(
            message: String,
            ex: Exception,
            vararg tags: String
    ) {
        critical(message, ex, tags.toList())
    }

    fun critical(
            message: String,
            ex: Exception,
            vararg custom: Pair<String, Any?>
    ) {
        critical(message, ex, custom = custom.toList())
    }

    fun critical(
            message: String,
            ex: Exception,
            vararg tags: String,
            custom: () -> Iterable<Pair<String, Any?>>
    ) {
        critical(message, ex, tags.toList(), custom())
    }

    fun critical(
            ex: Exception,
            tags: Iterable<String> = listOf(),
            custom: Iterable<Pair<String, Any?>> = listOf()
    ) {
        critical("The exception '${ex::class.qualifiedName}' was raised!", ex, tags, custom)
    }

    fun critical(
            ex: Exception,
            vararg tags: String
    ) {
        critical(ex, tags.toList())
    }

    fun critical(
            ex: Exception,
            vararg custom: Pair<String, Any?>
    ) {
        critical(ex, custom = custom.toList())
    }

    fun critical(
            ex: Exception,
            vararg tags: String,
            custom: () -> Iterable<Pair<String, Any?>>
    ) {
        critical(ex, tags.toList(), custom())
    }

    fun alert(
            message: String,
            tags: Iterable<String> = listOf(),
            custom: Iterable<Pair<String, Any?>> = listOf()
    ) {
        log(Severity.ALERT, message, tags, custom)
    }

    fun alert(
            message: String,
            vararg tags: String
    ) {
        alert(message, tags.toList())
    }

    fun alert(
            message: String,
            vararg custom: Pair<String, Any?>
    ) {
        alert(message, custom = custom.toList())
    }

    fun alert(
            message: String,
            vararg tags: String,
            custom: () -> Iterable<Pair<String, Any?>>
    ) {
        alert(message, tags.toList(), custom())
    }

    fun alert(
            message: String,
            ex: Exception,
            tags: Iterable<String> = listOf(),
            custom: Iterable<Pair<String, Any?>> = listOf()
    ) {
        alert(message, tags, custom + listOf(
                "exception" to ex::class.qualifiedName,
                "exceptionMessage" to ex.message,
                "stackTrace" to ex.stackTraceString
        ))
    }

    fun alert(
            message: String,
            ex: Exception,
            vararg tags: String
    ) {
        alert(message, ex, tags.toList())
    }

    fun alert(
            message: String,
            ex: Exception,
            vararg custom: Pair<String, Any?>
    ) {
        alert(message, ex, custom = custom.toList())
    }

    fun alert(
            message: String,
            ex: Exception,
            vararg tags: String,
            custom: () -> Iterable<Pair<String, Any?>>
    ) {
        alert(message, ex, tags.toList(), custom())
    }

    fun alert(
            ex: Exception,
            tags: Iterable<String> = listOf(),
            custom: Iterable<Pair<String, Any?>> = listOf()
    ) {
        alert("The exception '${ex::class.qualifiedName}' was raised!", ex, tags, custom)
    }

    fun alert(
            ex: Exception,
            vararg tags: String
    ) {
        alert(ex, tags.toList())
    }

    fun alert(
            ex: Exception,
            vararg custom: Pair<String, Any?>
    ) {
        alert(ex, custom = custom.toList())
    }

    fun alert(
            ex: Exception,
            vararg tags: String,
            custom: () -> Iterable<Pair<String, Any?>>
    ) {
        alert(ex, tags.toList(), custom())
    }

    fun emergency(
            message: String,
            tags: Iterable<String> = listOf(),
            custom: Iterable<Pair<String, Any?>> = listOf()
    ) {
        log(Severity.EMERGENCY, message, tags, custom)
    }

    fun emergency(
            message: String,
            vararg tags: String
    ) {
        emergency(
                message,
                tags.toList()
        )
    }

    fun emergency(
            message: String,
            vararg custom: Pair<String, Any?>
    ) {
        emergency(message, custom = custom.toList())
    }

    fun emergency(
            message: String,
            vararg tags: String,
            custom: () -> Iterable<Pair<String, Any?>>
    ) {
        emergency(message, tags.toList(), custom())
    }

    fun emergency(
            message: String,
            ex: Exception,
            tags: Iterable<String> = listOf(),
            custom: Iterable<Pair<String, Any?>> = listOf()
    ) {
        emergency(message, tags, custom + listOf(
                "exception" to ex::class.qualifiedName,
                "exceptionMessage" to ex.message,
                "stackTrace" to ex.stackTraceString
        ))
    }

    fun emergency(
            message: String,
            ex: Exception,
            vararg tags: String
    ) {
        emergency(message, ex, tags.toList())
    }

    fun emergency(
            message: String,
            ex: Exception,
            vararg custom: Pair<String, Any?>
    ) {
        emergency(message, ex, custom = custom.toList())
    }

    fun emergency(
            message: String,
            ex: Exception,
            vararg tags: String,
            custom: () -> Iterable<Pair<String, Any?>>
    ) {
        emergency(message, ex, tags.toList(), custom())
    }

    fun emergency(
            ex: Exception,
            tags: Iterable<String> = listOf(),
            custom: Iterable<Pair<String, Any?>> = listOf()
    ) {
        emergency("The exception '${ex::class.qualifiedName}' was raised!", ex, tags, custom)
    }

    fun emergency(
            ex: Exception,
            vararg tags: String
    ) {
        emergency(ex, tags.toList())
    }

    fun emergency(
            ex: Exception,
            vararg custom: Pair<String, Any?>
    ) {
        emergency(ex, custom = custom.toList())
    }

    fun emergency(
            ex: Exception,
            vararg tags: String,
            custom: () -> Iterable<Pair<String, Any?>>
    ) {
        emergency(ex, tags.toList(), custom())
    }

    fun <T> trace(name: String, vararg args: Pair<String, Any?>, logReturn: Boolean = false, call: () -> T): T {
        val formattedArgs = args.joinToString(", ") { (name, value) ->
            "$name -> $value"
        }
        info("calling $name with $formattedArgs", *args)
        var errorOccurred = false
        try {
            val result = call()
            if (logReturn) {
                info("calling $name with $formattedArgs was successful", *args, "returnedValue" to result)
            } else {
                info("calling $name with $formattedArgs was successful", *args)
            }
            return result
        } catch (e: Exception) {
            error("a ${e.javaClass.canonicalName} was raised upon calling $name with $formattedArgs", e, *args)
            errorOccurred = true
            throw e
        } finally {
            if (errorOccurred) {
                warn("calling $name with $formattedArgs resulted in an error, please check the previous logs")
            }
        }
    }

    fun <T> trace(vararg args: Pair<String, Any?>, logReturn: Boolean = false, call: () -> T): T {
        val callerName = try {
            val stack = Thread.currentThread().stackTrace.toList()
            val caller = stack.subList(1, stack.size).filter {
                !it.className.startsWith("co.stone.loggingkt")
            }.first {
                !(it.className.endsWith("Companion") && it.methodName == "trace")
            }
            "${caller.className}.${caller.methodName}#${caller.lineNumber}"
        } catch (e: Exception) {
            "UNKNOWN"
        }
        return trace(callerName, *args, logReturn = logReturn, call = call)
    }

    fun log(severity: Severity, message: String, tags: Iterable<String>, custom: Iterable<Pair<String, Any?>>)
}

