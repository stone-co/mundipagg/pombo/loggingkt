package co.stone.loggingkt.loggers

enum class Severity(val identifier: Int) {
    OFF(-1),
    EMERGENCY(0),
    ALERT(1),
    CRITICAL(2),
    ERROR(3),
    WARNING(4),
    NOTICE(5),
    INFO(6),
    DEBUG(7);

    companion object Resolver {
        fun get(identifier: Int): Severity {
            return values().first { it.identifier == identifier }
        }
    }
}