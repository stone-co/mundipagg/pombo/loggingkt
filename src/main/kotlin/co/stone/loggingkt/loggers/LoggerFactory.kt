package co.stone.loggingkt.loggers

import co.stone.loggingkt.handlers.LogHandler
import kotlin.reflect.KClass

internal object LoggerFactory {
    private val handlers = mutableListOf<LogHandler>()

    fun new(owner: Class<*>): Logger {
        return new(owner.canonicalName)
    }

    fun new(owner: KClass<*>): Logger {
        return new(owner.qualifiedName!!)
    }

    fun new(owner: String = "shared"): Logger {
        return SimpleLogger(owner, handlers)
    }

    fun addHandler(handler: LogHandler) {
        handlers.add(handler)
    }
}
