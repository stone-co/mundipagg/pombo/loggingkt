package co.stone.loggingkt.loggers

import co.stone.loggingkt.handlers.LogHandler


internal class SimpleLogger(
        private val owner: String,
        private val handlers: List<LogHandler>
) : Logger {

    override fun log(severity: Severity, message: String, tags: Iterable<String>, custom: Iterable<Pair<String, Any?>>) {
        this.handlers.parallelStream().forEach {
            try {
                it.handle(this.owner, severity, message, Thread.currentThread().id, tags, custom)
            } catch (_: Exception) {

            }
        }
    }
}