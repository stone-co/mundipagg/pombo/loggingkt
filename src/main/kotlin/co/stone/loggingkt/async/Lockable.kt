package co.stone.loggingkt.async

internal interface Lockable {
    fun <T> lock(call: Lockable.() -> T): T
}