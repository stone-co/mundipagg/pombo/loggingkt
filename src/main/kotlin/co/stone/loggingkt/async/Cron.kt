package co.stone.loggingkt.async

internal interface Cron {
    fun start()
    fun stop()
}