package co.stone.loggingkt.async

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import org.joda.time.Duration

internal class Every(private val every: Duration, action: () -> Any?) : AbstractCron(action) {
    override fun start() {
        launch(CommonPool) {
            while (true) {
                delay(every.millis)
                beat.send(true)
            }
        }
    }
}