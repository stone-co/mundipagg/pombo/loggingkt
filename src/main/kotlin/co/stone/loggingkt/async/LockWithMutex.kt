package co.stone.loggingkt.async

import kotlinx.coroutines.experimental.runBlocking
import kotlinx.coroutines.experimental.sync.Mutex

internal class LockWithMutex : Lockable {
    private val mutex = Mutex()
    override fun <T> lock(call: Lockable.() -> T): T {
        return runBlocking {
            mutex.lock()
            try {
                call()
            } finally {
                mutex.unlock()
            }
        }
    }
}