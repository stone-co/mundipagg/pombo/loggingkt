package co.stone.loggingkt.async

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.channels.Channel
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking

internal abstract class AbstractCron(private val action: () -> Any?) : Cron {
    protected val beat: Channel<Boolean> = Channel(1)
    private val worker = launch(CommonPool) {
        while (true) {
            beat.receive()
            action()
        }
    }

    override fun stop() {
        runBlocking {
            beat.close()
        }
    }

}