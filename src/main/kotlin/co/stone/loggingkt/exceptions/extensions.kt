package co.stone.loggingkt.exceptions

import kotlin.reflect.KClass


internal inline fun <T> neverThrows(ex: KClass<*> = Throwable::class, crossinline call: () -> T): T? {
    return try {
        call()
    } catch (e: Throwable) {
        if (!ex.isInstance(e)) {
            throw e
        }
        null
    }
}