# Loggingkt: Using Cerberus in a way that you don't want kill yourself

## Why?

Everyone needed, so let's make a package

## How?

```kotlin
class WithLog {
    companion object: Logger by LoggerFactory.create("WithLog")
    
    fun someMethod() {
        info("some log")
    }
}

```